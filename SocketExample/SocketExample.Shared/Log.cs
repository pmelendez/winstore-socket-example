﻿using System;
using System.Collections.Generic;
using System.Text;
using Windows.Storage;

namespace SocketExample
{
    class Log
    {
        private static Log _instance;
        private static string Filename = "SocketExample.log";
        private static StorageFile file;
        private static List<string> entries;
        private Log()
        {
            OpenFile();
        }

        ~Log()
        {
        }

        private void OpenFile()
        {
            try
            {
                var folder = ApplicationData.Current.LocalFolder;
                file = folder.CreateFileAsync(Filename, CreationCollisionOption.OpenIfExists).GetAwaiter().GetResult();
                entries = new List<string>();
                InitialLogMessage();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Error creating the log:" + ex.Message);
            }
        }

        private void InitialLogMessage()
        {
            Write("*** Starting Log Message - Usually this means the start of the application");

            if (System.Diagnostics.Stopwatch.IsHighResolution)
            {
                Write("This machine uses High Resolution for printing elapsed timing");
            }

            long nanosecPerTick = (1000L * 1000L * 1000L) / System.Diagnostics.Stopwatch.Frequency;

            Write(string.Format("Timer frequency in ticks per second = {0}", System.Diagnostics.Stopwatch.Frequency));
            Write(string.Format("Timer is accurate within {0} nanoseconds", nanosecPerTick));
        }

        public void Write(string entry)
        {
            if (file != null)
            {
                long ticks = DateTime.UtcNow.Ticks - DateTime.Parse("01/01/1970 00:00:00").Ticks;
                ticks /= 10000000; //Convert windows ticks to seconds
                var timestamp = ticks.ToString();
                var formatted_entry = string.Format("[{0}]    {1}", timestamp, entry);
                entries.Add(formatted_entry);

                FileIO.AppendTextAsync(file, formatted_entry + "\r\n").GetAwaiter().GetResult();
            }
        }

        public static Log I
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new Log();
                }
                return _instance;
            }
        }
    }
}
