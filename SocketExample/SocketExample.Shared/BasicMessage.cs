﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace SocketExample
{

    [DataContract]
    class BasicMessage : IMessage
    {
        [DataMember(Name = "Type")]
        public string Type { get; set; }        

    }
}
