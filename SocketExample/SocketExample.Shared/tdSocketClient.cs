﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Networking;
using Windows.Networking.Sockets;
using Windows.Storage.Streams;

namespace SocketExample
{
    class tdSocketClient
    {
        private StreamSocket clientSocket;
        private HostName serverHost;
        private string serverHostnameString = "tudomino.com";
        private string serverPort = "14095";
        private Dictionary<string,List<Action<string>>> callbacks;
        public bool IsReceiving { get; set; }
            
        public tdSocketClient()
        {
            serverHost = new HostName(serverHostnameString);
            clientSocket = new StreamSocket();
            callbacks = new Dictionary<string, List<Action<string>>>();
            StartConnection();
            StartReceiveLoop();
        }

        public void AddCallback(string type, Action<string> callback)
        {
            if (!callbacks.ContainsKey(type))
            {
                callbacks.Add(type, new List<Action<string>>());
            }

            callbacks[type].Add(callback);
        }

        public async void StartConnection()
        {
            // Try to connect to the 
            await clientSocket.ConnectAsync(serverHost, serverPort,SocketProtectionLevel.PlainSocket);
        }

        public async void Send(string sendData)
        {
            DataWriter writer = new DataWriter(clientSocket.OutputStream);
            //var len = writer.MeasureString(sendData); // Gets the UTF-8 string length.

            var len = writer.WriteString(sendData);
            // Call StoreAsync method to store the data to a backing stream
            var count = await writer.StoreAsync();
            var didFlush = await writer.FlushAsync();

            // detach the stream and close it
            writer.DetachStream();
            writer.Dispose();
        }

        public async void StartReceiveLoop()
        {
            IsReceiving = true;

            while (IsReceiving)
            {
                var msg = await Receive();

                if (msg != "")
                {
                    var objMsg = JSONHelper.FromJson<BasicMessage>(msg);

                    if (objMsg.Type != null && callbacks.ContainsKey(objMsg.Type))
                    {
                        var callbackList = callbacks[objMsg.Type];

                        foreach (var cb in callbackList)
                        {
                            cb(msg);
                        }
                    }
                    else
                    {
                        Log.I.Write("Couldn't find a proper handler for message:" + msg);
                    }
                }

                await Task.Delay(200);
            }
        }


        public async Task<string> Receive()
        {
            DataReader reader = new DataReader(clientSocket.InputStream);
            string dataContent ="";
            // Set inputstream options so that we don't have to know the data size
            reader.InputStreamOptions = InputStreamOptions.Partial;
            try
            {
                var count = await reader.LoadAsync(1024*1024*15);// Max: 15Mb
                dataContent = reader.ReadString(count);                
            }
            catch(Exception ex)
            {
                Log.I.Write("Couldn't read from socket stream: " + ex.Message);
            }

            return dataContent;
        }

    }
    
}
