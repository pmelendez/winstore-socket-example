﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace SocketExample
{
    [DataContract]
    class ChatMessage : IMessage
    {
        [DataMember(Name = "Type")]
        public string Type { get; set; }

        [DataMember(Name = "MessageText")]
        public string MessageText { get; set; }

    }
}
