﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Collections;

namespace SocketExample
{
    class JSONHelper
    {
        public static T FromJson<T>(string jsonStr) where T : new()
        {
            T serializedObj;
            try
            {
                Log.I.Write(string.Format("FromJson call: {0} ", jsonStr.Substring(0, 20)));

                using (var memStream = new MemoryStream())
                {
                    var writer = new StreamWriter(memStream);
                    writer.Write(jsonStr);
                    writer.Flush();
                    memStream.Position = 0;
                    var ser = new DataContractJsonSerializer(
                                                           typeof(T),
                                                           new DataContractJsonSerializerSettings
                                                           {
                                                               UseSimpleDictionaryFormat = true
                                                           });

                    serializedObj = (T)ser.ReadObject(memStream);
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                serializedObj = new T();
                Log.I.Write(string.Format("Exception on FromJson call: {0} ", s));
            }

            return serializedObj;
        }

        //PM@TODO We are deprecating this method.

        public static T FromJson<T>(Stream json) where T : new()
        {
            T serializedObj;
            try
            {
                var sw = new StreamReader(json, Encoding.UTF8);
                string jsonStr = sw.ReadToEnd();
                Log.I.Write(string.Format("FromJson call: {0} ", jsonStr.Substring(0, 20)));

                using (var memStream = new MemoryStream())
                {
                    var writer = new StreamWriter(memStream);
                    writer.Write(jsonStr);
                    writer.Flush();
                    memStream.Position = 0;
                    var ser = new DataContractJsonSerializer(
                                                           typeof(T),
                                                           new DataContractJsonSerializerSettings
                                                           {
                                                               UseSimpleDictionaryFormat = true
                                                           });

                    serializedObj = (T)ser.ReadObject(memStream);
                }
                
            }
            catch (Exception e)
            {
                string s = e.Message;
                serializedObj = new T();
                Log.I.Write(string.Format("Exception on FromJson call: {0} ", s));
            }

            return serializedObj;
        }

        public static string CleanHTMLString(string html)
        {
            return html.Replace("\"", "'");
        }

        public static string CollectionToJSON<T>(IList collection) where T : new()
        {
            var sb = new StringBuilder();
            sb.Append("[");

            if (collection != null)
            {
                var numItems = collection.Count;

                for (int i = 0; i < numItems; i++)
                {
                    dynamic item = collection[i];
                    sb.Append(item.ToJSON());
                    if (i < numItems - 1)
                    {
                        sb.Append(",");
                    }
                }
            }

            sb.Append("]");

            return sb.ToString();
        }
    }
}
