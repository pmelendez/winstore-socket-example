﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace SocketExample
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        tdSocketClient client;
        public MainPage()
        {
            this.InitializeComponent();
            textBox.Text = "";
            textBlock.Text = "Enter text in the textbox and hit \"send\" \n";
            client = new tdSocketClient();
            client.AddCallback("chat", 
                (string msg) => 
                {
                    var message = JSONHelper.FromJson<ChatMessage>(msg);
                    textBlock.Text += message.MessageText + "\n";
                } );
        }

        private void button_PointerPressed(object sender, PointerRoutedEventArgs e)
        {
            
        }

        private void button_Tapped(object sender, TappedRoutedEventArgs e)
        {
            //textBlock.Text += textBox.Text + "\n";
            string format = "{{ \"Type\": \"chat\", \"MessageText\":\"{0}\"}}";
            client.Send(string.Format(format, textBox.Text));
            textBox.Text = "";
        }
    }
}
